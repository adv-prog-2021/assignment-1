# Assignment 1: Git
Welcome to you first assignment on git. This assignment is about: clone, create branch, modify, add, commit, push and merge.

## What you should do?

To complete this assignemnt, you perform the following steps:

1. Start by cloning this project to your working directory
2. Create a branch with your full name like: ali-komaty
3. Add a text file with `.txt` extension and name it the same as your full name. For example, I would name my file as `ali-komaty.txt`
4. Modify the file named `names.txt` by adding your full name at the end of it.
5. Commit your changes to you branch
6. Push your changes to your branch
7. Merge your changes with master
